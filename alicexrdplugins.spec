Summary: ALICE xrootd plugins meta-package
Name: alicexrdplugins
Version: 0.4
Release: 1%{?dist}
License: none
Group:   Applications/xrootd

Requires: xrootd-aggregatingname2name >= 1.0.1-2
Requires: alicetokenacc
Obsoletes: tokenauthz, xrootd-alicetokenacc
Conflicts: tokenauthz, xrootd-alicetokenacc

%description
ALICE xrootd plugins meta-package

%prep
%setup -c -T

%install

%build

%files

%changelog
* Fri May 12 2023 Adrian Sevcenco <adrian.sevcenco@cern.ch> 0.4
- fix Conflicts/Obsoletes
* Wed Apr 20 2022 Adrian Sevcenco <adrian.sevcenco@cern.ch> 0.3
- replace old tokenauthz,xrootd-alicetokenacc with the new consolidated alicetokenacc
* Tue Feb 9 2021 Adrian Sevcenco <adrian.sevcenco@cern.ch> 0.2
- Remove clean Requires list
* Fri Jun 19 2015 Adrian Sevcenco <adrian.sevcenco@cern.ch> 0.1
- Initial package
